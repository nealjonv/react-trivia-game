React Trivia Game

To run, you need to run 'npm install' to manage dependencies, then run 'npm start' to kick off the build process. Then, just open a browser to your local triviaGame.html page.

Application uses React as the view layer, Flux as the control layer, and Backbone models as the data store.

This application was developed by Jon Neal as a learning tool into React and Flux. Live streaming of this development was done on twitch.tv/godofrabbits.