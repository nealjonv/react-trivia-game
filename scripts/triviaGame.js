var Dispatcher = require('flux').Dispatcher;
var React = require('react');
var $ = require('jQuery');
var _ = require('underscore');
var Backbone = require('backbone');

var TriviaQuestion = React.createClass({
	constants: {
		SWIPE_DISTANCE_THRESHOLD: .25,
		MAX_COLOR: 255
	},

	render: function() {
		return (
			<div className="triviaCard" onTouchStart={this.onTouchStart} onTouchMove={this.onTouchMove} onTouchEnd={this.onTouchEnd}>
				<h2>{this.props.question}</h2>
				<p>Swipe Right for Yes, Left for No</p>
			</div>
		);
	},

	onTouchStart: function(e) {
		this.startX = e.touches[0].pageX;
	},

	onTouchMove: function(e) {

		var xDelta = e.changedTouches[0].pageX - this.startX;
		var screenWidth = $('#container').width();
		var swipeDistance = Math.abs(xDelta) / screenWidth;
		if (swipeDistance > this.constants.SWIPE_DISTANCE_THRESHOLD)
			swipeDistance = this.constants.SWIPE_DISTANCE_THRESHOLD;

		var swipeColor = Math.round(this.constants.MAX_COLOR - ((swipeDistance / this.constants.SWIPE_DISTANCE_THRESHOLD) * this.constants.MAX_COLOR));
		var swipeColorRGB = (xDelta >= 0 ? [ swipeColor, this.constants.MAX_COLOR, swipeColor ] : [ this.constants.MAX_COLOR, swipeColor, swipeColor ]).join(',');

		$('.triviaCard').css('backgroundColor', 'rgb(' + swipeColorRGB + ')');
	},

	onTouchEnd: function(e) {

		var xDelta = e.changedTouches[0].pageX - this.startX;
		var swipeDirection = xDelta > 0 ? 'right' : 'left';
		var screenWidth = $('#container').width();

		if ((Math.abs(xDelta) / screenWidth) > this.constants.SWIPE_DISTANCE_THRESHOLD)
		{
			var answer = xDelta > 0 ? true : false;
			gameDispatcher.dispatch({
				actionType: 'answer-question',
				id: this.props.id,
				answer: answer
			});
		}
		else
		{
			$('.triviaCard').css('backgroundColor', '#fff');
		}
	}

});

var ScoreView = React.createClass({
	render: function() {
		return (
			<div className="triviaCard">
				<h2>Final Score: {this.props.score.total} / {this.props.score.possible}</h2>
			</div>
		);
	}
});

var ScoreMiniView = React.createClass({
	render: function() {
		var score = this.props.score.possible > 0 ? 'Score: ' + this.props.score.total + ' / ' + this.props.score.possible : '';
		return (
			<div className="scoreMiniView">{score}</div>
		);
	}
});

var GameContainer = React.createClass({

	componentWillMount: function() {

		gameStore.set('triviaQuestions', this.props.data);

		gameStore.on('change:triviaQuestions', function() {
			this.setState(this.state);
		}, this);
	},
	render: function() {

		var questions = gameStore.get('triviaQuestions');

		if (questions.length == 0)
		{
			return (<ScoreView score={gameStore.get('score')} />);
		}
		else
		{
			var currentQuestion = questions[0];
			return (
				<div className="container">
					<TriviaQuestion key={currentQuestion.id} {... currentQuestion} />
					<ScoreMiniView score={gameStore.get('score')} />
				</div>
			);
		}
	}
});

var gameDispatcher = new Dispatcher();

var GameStore = Backbone.Model.extend({
	defaults: {
		score: { total: 0, possible: 0 },
		triviaQuestions: []
	},
	answerIsCorrect: function(questionId, answer) {
		return _.find(this.get('triviaQuestions'), function(triviaQuestion) { return triviaQuestion.id == questionId; }).answer == answer;
	}
});
var gameStore = new GameStore();

gameDispatcher.register(function(action) {

	switch(action.actionType) {
		case 'answer-question':
			var toAdd = gameStore.answerIsCorrect(action.id, action.answer) ? 1 : 0;
			gameStore.set({
				score: { total: gameStore.get('score').total + toAdd, possible: gameStore.get('score').possible + 1 },
				triviaQuestions: _.filter(gameStore.get('triviaQuestions'), function(triviaQuestion) { return triviaQuestion.id != action.id })
			});
			break;
		default:
			// no-op
	}
});

var triviaQuestions = [
	{ id: 0, question: 'Does the earth revolve around the sun?', answer: true },
	{ id: 1, question: 'Do triangles have seven sides?', answer: false },
	{ id: 2, question: 'Is the square root of 49 equal to 11?', answer: false }
];

React.initializeTouchEvents(true);
React.render(<GameContainer data={triviaQuestions} />, $('body')[0]);
